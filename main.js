$(document).ready(function(){
  var allSolution = [
    [11, 12, 13],
    [21, 22, 23],
    [31, 32, 33],
    [11, 21, 31],
    [12, 22, 32],
    [13, 23, 33],
    [11, 22, 33],
    [13, 22, 31]
  ];
  createTable();
  function createTable (){
    for ( var i=1; i<=3; i++){
      $('.tictactoe').append('<div id="'+i+'" class="row"></div>');
      for (var j=1; j<=3; j++){
        $('.tictactoe #'+i).append('<div id="'+i+''+j+'" class="cell"></div>');
      }
    }
  }
  var piece = 'x';
  	var count = 0;
  $('.cell').on('click', function(){
    if(!$(this).hasClass('x') && !$(this).hasClass('o') && !$('.result').hasClass('win')){
      $(this).addClass(piece);

      win(piece);

      if(piece == 'x'){
        piece = 'o';
      } else {
        piece = 'x';
      }
      	count++;
    }
  });

  function win(piece){
    for (var i=0; i<allSolution.length; i++){
      if ($('#'+allSolution[i][0]).hasClass(piece) && $('#'+allSolution[i][1]).hasClass(piece) && $('#'+allSolution[i][2]).hasClass(piece)) {
        $('.result').addClass('win').text(piece + ' win!');
      }
      if(count == 8) {
        $('.result').addClass('win').text('draw!');
      }
    }
  }
  $('.reset').on('click', function() {
    count = 0;
    $('.cell').removeClass('x o');
    $('.result').removeClass('win').text('');
    piece = 'x';
  });
});
